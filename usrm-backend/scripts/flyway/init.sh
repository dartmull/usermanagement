#!/bin/bash

# This script initializes the database for flyway usage (it adds flyway schema_version table)

. $(dirname $(realpath $0))/config.sh

flyway -user=$DB_USER -password=$DB_PASSWORD -url=$DB_URL baseline
