#!/bin/bash

# This file runs the flyway migration command, to apply all sql scripts that are not already applied

. $(dirname $(realpath $0))/config.sh

flyway -user=$DB_USER -password=$DB_PASSWORD -url=$DB_URL \
       -locations=filesystem:$RELATIVE_DB_SCRIPTS_DIR migrate
