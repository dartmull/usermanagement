package com.kirild.userman.user.persistence;

import com.kirild.userman.user.common.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserTestManager {
    public static final String DEFAULT_USER_EMAIL = "d.j.trump@gmail.com";
    public static final String DEFAULT_USER_FIRST_NAME = "Donald";
    public static final String DEFAULT_USER_LAST_NAME = "Trump";

    @Autowired
    private UserCrudRepository userCrudRepository;

    /**
     * Method for cleaning the database. It is useful for preparation of clean environment for running tests.
     */
    public void cleanDb() {
        userCrudRepository.deleteAll();
    }

    public void prepareUserDbData() {
        User user = User.builder()
                .email(UserTestManager.DEFAULT_USER_EMAIL)
                .firstName(UserTestManager.DEFAULT_USER_FIRST_NAME)
                .lastName(UserTestManager.DEFAULT_USER_LAST_NAME)
                .build();
        userCrudRepository.save(new UserEntity(user));
    }

    public boolean isUserExisting(Integer id) {
        return userCrudRepository.existsById(id);
    }
}
