package com.kirild.userman.user.service;

import com.kirild.userman.user.BaseTests;
import com.kirild.userman.user.common.User;
import com.kirild.userman.user.persistence.UserTestManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@Test
public class UserServiceTest extends BaseTests {
    @Autowired
    private UserTestManager userTestManager;

    @Autowired
    private UserService userService;

    @BeforeMethod
    public void initialize() {
        userTestManager.prepareUserDbData();
    }

    @Test
    public void getUserByEmail_GotExpectedUser_Success() {
        User user = userService.getUserByEmail(UserTestManager.DEFAULT_USER_EMAIL);

        assertThat(user.getFirstName(), is(UserTestManager.DEFAULT_USER_FIRST_NAME));
        assertThat(user.getLastName(), is(UserTestManager.DEFAULT_USER_LAST_NAME));
    }

    @Test
    public void deleteUser_UserIsDeleted_Success() {
        User user = userService.getUserByEmail(UserTestManager.DEFAULT_USER_EMAIL);
        userService.deleteUser(user.getId());

        assertThat(userTestManager.isUserExisting(user.getId()), is(false));
    }

    @Test
    public void createUser_UserIsCreated_Success() {
        User existingUser = userService.getUserByEmail(UserTestManager.DEFAULT_USER_EMAIL);
        userService.deleteUser(existingUser.getId());
        User user = User.builder()
                .email(UserTestManager.DEFAULT_USER_EMAIL)
                .firstName(UserTestManager.DEFAULT_USER_FIRST_NAME)
                .lastName(UserTestManager.DEFAULT_USER_LAST_NAME)
                .build();
        User createdUser = userService.createUser(user);


        assertThat(user.getFirstName(), is(equalTo(createdUser.getFirstName())));
        assertThat(user.getLastName(), is(equalTo(createdUser.getLastName())));
        assertThat(user.getEmail(), is(equalTo(createdUser.getEmail())));
    }

    @Test
    public void updateUser_UserFirstNameIsUpdated_Success() {
        String newFirstName = "Donald J.";
        User user = userService.getUserByEmail(UserTestManager.DEFAULT_USER_EMAIL);
        user.setFirstName(newFirstName);
        User updateUser = userService.updateUser(user);

        assertThat(updateUser.getFirstName(), is(equalTo(newFirstName)));
    }
}
