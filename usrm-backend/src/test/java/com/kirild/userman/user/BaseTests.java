package com.kirild.userman.user;

import com.kirild.userman.config.MainConfig;
import com.kirild.userman.user.persistence.UserTestManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.AfterMethod;

import java.io.IOException;

@TestExecutionListeners(listeners = {
        ServletTestExecutionListener.class,
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
})
@WebAppConfiguration
@SpringBootTest(classes = MainConfig.class)
public class BaseTests extends AbstractTestNGSpringContextTests {
    @Autowired
    private UserTestManager userTestManager;

    @AfterMethod
    protected void deleteObjects() throws IOException {
        userTestManager.cleanDb();
    }
}
