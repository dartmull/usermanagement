package com.kirild.common.persistence;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.springframework.util.StringUtils;

import java.util.Locale;

/**
 * This class will be used as naming strategy for the hibernate identifiers. We need this strategy,
 * because hibernate doesn't support camelCase strategy after version 5.
 */
public class CamelCaseNamingStrategy implements PhysicalNamingStrategy {
    @Override
    public Identifier toPhysicalCatalogName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    @Override
    public Identifier toPhysicalColumnName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    @Override
    public Identifier toPhysicalSchemaName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    @Override
    public Identifier toPhysicalSequenceName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    @Override
    public Identifier toPhysicalTableName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    private Identifier convert(Identifier identifier) {
        if (identifier == null || StringUtils.isEmpty(identifier.render())) {
            return identifier;
        }

        String regex = "([a-z]|[0-9]+)([A-Z]|[0-9]+)";
        String replacement = "$1_$2";

        return Identifier.toIdentifier(identifier.render().replaceAll(regex, replacement).toLowerCase(Locale.ENGLISH));
    }
}
