package com.kirild.common.persistence;

import org.hibernate.QueryException;
import org.hibernate.dialect.MySQL57Dialect;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.dialect.function.TemplateRenderer;
import org.hibernate.engine.spi.Mapping;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import java.util.List;

public class MySqlCustomDialect extends MySQL57Dialect {
    public MySqlCustomDialect() {
        registerFunction("matches",
                new MatchAgainstSqlFunctionTemplate(StandardBasicTypes.DOUBLE));
    }

    private static class MatchAgainstSqlFunctionTemplate implements SQLFunction {
        private final Type type;
        private final boolean hasParenthesesIfNoArgs;

        public MatchAgainstSqlFunctionTemplate(Type type, boolean hasParenthesesIfNoArgs) {
            this.type = type;
            this.hasParenthesesIfNoArgs = hasParenthesesIfNoArgs;
        }

        public MatchAgainstSqlFunctionTemplate(Type type) {
            this(type, true);
        }

        @Override
        public boolean hasArguments() {
            return true;
        }

        @Override
        public boolean hasParenthesesIfNoArguments() {
            return hasParenthesesIfNoArgs;
        }

        @Override
        public Type getReturnType(Type type, Mapping mapping) throws QueryException {
            return this.type;
        }

        @Override
        public String render(Type type, List args, SessionFactoryImplementor factory) throws QueryException {
            final String separator = ", ";
            StringBuilder sqlTemplate = new StringBuilder();

            if (args.size() < 2) {
                throw new IllegalArgumentException("You need to provide at least 2 arguments when using the"
                        + " 'matches'  SQL function.");
            }

            sqlTemplate.append("MATCH (");

            for (int i = 0; i < args.size() - 1; i++) {
                if (i > 0) {
                    sqlTemplate.append(separator);
                }

                sqlTemplate.append("?").append(i + 1);
            }

            sqlTemplate
                    .append(") AGAINST (?")
                    .append(args.size())
                    .append(" IN BOOLEAN MODE");

            final TemplateRenderer renderer = new TemplateRenderer(sqlTemplate.toString());

            return renderer.render(args, factory);
        }
    }
}
