package com.kirild.common.utils;

/**
 * This interface exposes functionality for converting the concrete object to some POJO.
 *
 * @param <PojoT> the POJO type to which we can convert the concrete object.
 */
public interface PojoConvertible<PojoT> {
    PojoT toPojo();
}
