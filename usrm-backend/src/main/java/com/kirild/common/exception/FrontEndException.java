package com.kirild.common.exception;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

// This will disable the auto serialization and let us to control what to serialize.
@JsonAutoDetect(fieldVisibility = Visibility.NONE, getterVisibility = Visibility.NONE)
@JsonInclude(Include.NON_NULL)
public class FrontEndException extends RuntimeException {
    private static final long serialVersionUID = -3203283967866025541L;

    @Setter
    private boolean shouldSerializeStackTrace = false;

    @Getter
    private final HttpStatus httpStatus;

    @Getter
    @JsonProperty
    private final Object code;

    public FrontEndException(HttpStatus status, Object errorCode, String message) {
        super(message);

        httpStatus = status;
        code = errorCode;
    }

    @JsonProperty
    @Override
    public StackTraceElement[] getStackTrace() {
        return shouldSerializeStackTrace ? super.getStackTrace() : null;
    }
}
