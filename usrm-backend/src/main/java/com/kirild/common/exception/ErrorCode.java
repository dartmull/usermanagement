package com.kirild.common.exception;

public enum ErrorCode {
    RESOURCE_NOT_FOUND,
    INTERNAL_SERVER_ERROR
}
