package com.kirild.common.exception;

import org.springframework.http.HttpStatus;

public class InternalServerException extends FrontEndException {
    private static final long serialVersionUID = 2435342420746960823L;

    public InternalServerException(String message) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, ErrorCode.INTERNAL_SERVER_ERROR, message);
    }
}
