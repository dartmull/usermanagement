package com.kirild.common.exception;

import org.springframework.http.HttpStatus;

public class ResourceNotFoundException extends FrontEndException {
    private static final long serialVersionUID = -1971946529033550378L;

    private final Object type;
    private final Object qualifier;

    public ResourceNotFoundException(Object qualifier, Object type, String message) {
        super(HttpStatus.NOT_FOUND, ErrorCode.RESOURCE_NOT_FOUND, message);

        this.type = type;
        this.qualifier = qualifier;
    }
}
