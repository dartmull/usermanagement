package com.kirild.userman.user.persistence;

import com.kirild.common.exception.ResourceNotFoundException;
import com.kirild.common.exception.ResourceType;
import com.kirild.userman.user.common.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public class UserRepository {
    @Autowired
    private UserCrudRepository userCrudRepository;

    /**
     * Function that stores User DTO in the database.
     *
     * @param user the passed user DTO.
     *
     * @return the created User DTO.
     */
    public User createUser(User user) {
        return userCrudRepository.save(new UserEntity(user)).toPojo();
    }

    /**
     * Function that deletes user by id in the database.
     *
     * @param id the id of the user in the database.
     * @throws ResourceNotFoundException if there is no user with passed id in the database.
     */
    @Transactional
    public void deleteUser(Integer id) {
        userCrudRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id, ResourceType.USER, "No user with id: " + id));
        userCrudRepository.deleteById(id);
    }

    /**
     * Function that updates the user in the database.
     *
     * @param user the new User DTO with the updated properties.
     * @throws ResourceNotFoundException if there is no user with passed id in the database.
     *
     * @return the updated User DTO.
     */
    @Transactional
    public User updateUser(User user) {
        Integer id = user.getId();
        UserEntity userEntity = userCrudRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id, ResourceType.USER, "No user with id: " + id));
        userEntity.update(user);

        return userCrudRepository.save(userEntity).toPojo();
    }

    /**
     * Function that returns User DTO by email.
     *
     * @param email the email of the user.
     * @throws ResourceNotFoundException if there is no user with passed email in the database.
     *
     * @return the User DTO.
     */
    public User getUserByEmail(String email) {
        return userCrudRepository
                .findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException(
                        email,
                        ResourceType.USER,
                        "No user with email: " + email))
                .toPojo();
    }

    public Optional<UserEntity> isUserExisting(String email) {
        return userCrudRepository.findByEmail(email);
    }
}
