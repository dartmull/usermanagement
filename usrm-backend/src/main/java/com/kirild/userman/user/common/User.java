package com.kirild.userman.user.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    Integer id;
    String email;
    String firstName;
    String lastName;
    Instant creationDate;
}
