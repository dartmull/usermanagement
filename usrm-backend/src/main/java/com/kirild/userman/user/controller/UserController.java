package com.kirild.userman.user.controller;

import com.kirild.userman.user.common.User;
import com.kirild.userman.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    private static final String CREATE = "/create";
    private static final String UPDATE = "/update";
    private static final String DELETE = "/delete";

    @Autowired
    private UserService userService;

    @ResponseStatus(value = HttpStatus.CREATED)
    @RequestMapping(value = UserController.CREATE, method = RequestMethod.POST)
    public User createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @RequestMapping(value = UserController.UPDATE, method = RequestMethod.PUT)
    public User updateUser(@RequestBody User user) {
        return userService.updateUser(user);
    }

    @RequestMapping(value = UserController.DELETE, method = RequestMethod.DELETE)
    public void deleteUser(@RequestParam Integer id) {
        userService.deleteUser(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public User getUser(@RequestParam String email) {
        return userService.getUserByEmail(email);
    }
}
