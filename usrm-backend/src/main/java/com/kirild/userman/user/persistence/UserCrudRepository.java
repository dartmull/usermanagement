package com.kirild.userman.user.persistence;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

interface UserCrudRepository extends CrudRepository<UserEntity, Integer> {
    Optional<UserEntity> findByEmail(String email);
}
