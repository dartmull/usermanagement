package com.kirild.userman.user.service;

import com.kirild.common.exception.InternalServerException;
import com.kirild.common.exception.ResourceNotFoundException;
import com.kirild.userman.user.common.User;
import com.kirild.userman.user.persistence.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private static boolean isEmailFormatValid(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    /**
     * Function that creates user in the database. It validates the email property of the User DTO and verifies that
     * there is no existing user with such email in the database already.
     *
     * @param user the User DTO.
     * @throws InternalServerException if the email exists or is invalid.
     *
     * @return the created User DTO
     */
    @Transactional
    public User createUser(User user) {
        if (!UserService.isEmailFormatValid(user.getEmail())) {
            log.error("Invalid email format while trying to create user.");
            throw new InternalServerException("Invalid email format.");
        } else if (userRepository.isUserExisting(user.getEmail()).isPresent()) {
            log.error("Existing email while trying to create user.");
            throw new InternalServerException("Email already exists.");
        } else {
            return userRepository.createUser(user);
        }
    }

    /**
     * Function that deletes the User in the database.
     *
     * @param id the id of the User in the database.
     * @throws InternalServerException if there is no user with such id in the database.
     */
    public void deleteUser(Integer id) {
        try {
            userRepository.deleteUser(id);
        } catch (ResourceNotFoundException exception) {
            log.error("No user in the database with id: " + id, exception);
            throw new InternalServerException("There is no existing user in the database with id: " + id);
        }
    }

    /**
     * Function that updates User DTO.
     *
     * @param user the User DTO with updated properties.
     * @throws InternalServerException if there is no such user in the database.
     *
     * @return the update User DTO.
     */
    public User updateUser(User user) {
        try {
            return userRepository.updateUser(user);
        } catch (ResourceNotFoundException exception) {
            log.error("No such user in the database: " + user.toString(), exception);
            throw new InternalServerException("There is no such user in the database: " + user.toString());
        }
    }

    /**
     * Function that returns User DTO by passing email.
     *
     * @param email the email of the User.
     * @throws InternalServerException if the email format is wrong or there is no user with such email in the database.
     *
     * @return the found User DTO.
     */
    public User getUserByEmail(String email) {
        if (UserService.isEmailFormatValid(email)) {
            try {
                return userRepository.getUserByEmail(email);
            } catch (ResourceNotFoundException exception) {
                log.error("No user in the database with email: " + email, exception);
                throw new InternalServerException("There is no existing user in the database with email: " + email);
            }
        } else {
            log.error("No user in the database with email: " + email);
            throw new InternalServerException("Invalid email format.");
        }
    }
}
