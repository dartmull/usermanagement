package com.kirild.userman.user.persistence;

import com.kirild.common.utils.PojoConvertible;
import com.kirild.userman.user.common.User;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.Instant;
import java.util.Date;

@NoArgsConstructor
@Entity
@Table(name = "he_user")
class UserEntity implements PojoConvertible<User> {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(length = 45)
    private String firstName;

    @Column(length = 45)
    private String lastName;

    @Column(nullable = false)
    private Instant creationDate;

    public UserEntity(Integer id, String email, String firstName, String lastName, Instant creationDate) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.creationDate = creationDate;
    }

    UserEntity(User user) {
        this.email = user.getEmail();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.creationDate = new Date().toInstant();
    }

    void update(User user) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
    }

    @Override
    public User toPojo() {
        return User
                .builder()
                .id(id)
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .creationDate(creationDate)
                .build();
    }
}
