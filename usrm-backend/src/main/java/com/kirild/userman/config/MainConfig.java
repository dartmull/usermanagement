package com.kirild.userman.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.time.Clock;

@Configuration
@EnableAutoConfiguration
@ComponentScan("com.kirild")
@EnableJpaRepositories({"com.kirild.userman.*.persistence"})
@EnableTransactionManagement
@EnableJpaAuditing
public class MainConfig {
    @Bean
    public Clock clock() {
        return Clock.systemUTC();
    }
}
