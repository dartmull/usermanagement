-- The user table is named "he_user" (holiday extras user), because "user" is reserved word in MySQL
CREATE TABLE `he_user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `creation_date` TIMESTAMP NOT NULL,
  UNIQUE (`email`),
  PRIMARY KEY (`id`));
