# INSTALLATION STEPS

1. Download and install JDK 8u201 https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

2. Download and install MySQL 5.7.25 https://dev.mysql.com/downloads/windows/installer/5.7.html

3. Download and install IntelliJ IDEA Community https://www.jetbrains.com/idea/download/#section=windows

4. Download and install Git Bash https://git-scm.com/downloads

5. Install Lombok plugin for IntelliJ IDEA

# CONFIGURATION STEPS

1. Checkout the project from the repository: git clone git@bitbucket.org:dartmull/usermanagement.git

2. Create database with the following command: CREATE DATABASE `usermanagement`;

3. Create database user with the following commands:
   CREATE USER 'usermanagement_admin'@'localhost' IDENTIFIED BY 'EA0C62E40FEA1EEC7DECC860EDB684CCCBE042ED12802C6EC74B95D7C09AD10F';
   GRANT ALL PRIVILEGES ON *.* TO 'usermanagement_admin'@'localhost';

4. Open IntelliJ IDEA, import the checkout project, open the built-in maven tab and run clean install.

5. Build the project by running maven clean install from the maven menu in IntelliJ IDEA

6. Run the project with the following maven configuration:
   clean spring-boot:run "-Dspring-boot.run.jvmArguments=-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 -Dspring.profiles.active=local"

# TESTS

1. Go to file usermanagement\usrm-backend\src\test\java\com\kirild\userman\user\service\UserServiceTest.java
   Right click on the file and press Run. If you have two options choose TestNG.

2. Go to main/resources directory where you will find postman file with prepared requests.
